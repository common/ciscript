if [ -d ../.docker ]; then

    cmake --build . --target install \
          --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}

    if [ -z ${PROJECT_NAME} ]; then
        cd ..
        PROJECT_NAME=`grep project CMakeLists.txt | grep VERSION | cut -d"(" -f2 | sed -e "s/ //" | cut -d " " -f1 | sed -e "s/VERSION//"`
        cd -
    fi

    # Converting project namoe to lower case
    IMAGE_NAME=$(echo ${PROJECT_NAME} | tr '[:upper:]' '[:lower:]')

    if [ -z ${APP_VERSION} ]; then
        cd ..
        APP_VERSION=$(grep VERSION CMakeLists.txt  | grep project | sed 's/.*VERSION //' | cut -d ")" -f1 | cut -d" " -f1)
        cd -
    fi

    INSTALL_DIR=$(pwd)/install
    cd ../.docker
    cp -rp ${INSTALL_DIR} .

    # Deploy Qt libs if QTDIR is present (non apt installed installation)
    if [ ! -z ${QTDIR} ]; then

        # If no app defined use project name as default
        # (ToDo: maybe use specific docker config instead of AppImage config
        if [ -z "${APPIMAGES}" ]; then
            APPIMAGES=${PROJECT_NAME};
        fi

        export QTBINPATH=${QTDIR}/bin/
        QMAKE=$(PATH=$QTBINPATH:$PATH which qmake)
        # Download latest linuxdeployqt
        wget https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage
        chmod +x linuxdeployqt-continuous-x86_64.AppImage
        # Run deploy tool for each app
        for APP in ${APPIMAGES}; do
            PATH=$QTBINPATH:$PATH VERSION=test QT_PLUGIN_PATH= ./linuxdeployqt-continuous-x86_64.AppImage install/bin/${APP} -qmake=$QMAKE
        done
    fi

    for  df in $(ls Dockerfile_*); do

        echo Processing $df
        DOCKER_IMAGE_SUFFIX=$(echo ${df} | cut -d "_" -f2)
        DOCKER_IMAGE=${IMAGE_NAME}:${APP_VERSION}-${DOCKER_IMAGE_SUFFIX}

        docker build --network=host --tag vglab/${DOCKER_IMAGE} -f ${df} .

        cd -
        DOCKER_TARGZ="docker-${DOCKER_IMAGE}.tar.gz"
        docker save vglab/${DOCKER_IMAGE} | gzip > ${DOCKER_TARGZ}

        ssh $(echo ${DOCKER_REPO} | cut -d: -f1) mkdir -p $(echo ${DOCKER_REPO} | cut -d: -f2)/${PROJECT_NAME}/${APP_VERSION}
        scp ./${DOCKER_TARGZ} ${DOCKER_REPO}/${PROJECT_NAME}/${APP_VERSION}

        # Push to Docker Hub (only if not dev intermediate release)
        if [ $(echo ${APP_VERSION} | cut -d"-" -f1) != "dev" ]; then
            ~/.docker-login.sh 
            docker push vglab/${DOCKER_IMAGE}
        fi
        
        cd ../.docker

        docker image rm vglab/${DOCKER_IMAGE}

    done

    rm -rf install
    cd -
fi
