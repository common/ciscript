# Ensure installation
cmake --build . --target install \
      --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}
# Patch rpath for exectuable

# If no app defined use project name as default
if [ -z ${APPBUNDLES} ]; then
    APPBUNDLES=${PROJECT_NAME};
fi

for APP in $APPBUNDLES; do

    install_name_tool -add_rpath "@executable_path/../Frameworks" \
                      install/bin/${APP}.app/Contents/MacOS/${APP}

    # WAR for hdf5
    cd install/bin/${APP}.app/Contents/MacOS
    hdf5cppdylib=$(otool -L ${APP} | grep "libhdf5_cpp" | cut -d"/" -f2- | cut -d" " -f1)
    if [ ! -z ${hdf5cppdylib} ]; then
        hdf5dylib="/"$(otool -L $hdf5cppdylib | tail -n +2 | grep "libhdf5\." | grep -v executable_path | cut -d"/" -f2- | cut -d" " -f1)
        if [ ! -z ${hdf5dylib} ]; then
            install_name_tool -change $hdf5dylib \
                              @executable_path/../Frameworks/$(basename $hdf5dylib) \
                              $hdf5cppdylib
        fi
    fi
    cd -

    # WAR for QGLViewer
    cd install/bin/${APP}.app/Contents/MacOS
    APP_EXE=$(grep BundleExecutable ../Info.plist -A1 | grep string | cut -d ">" -f2 | cut -d "<" -f1)

    qglviewerframework=$(otool -L ${APP_EXE} | grep QGLViewer | cut -d " " -f1 )
    if [ ! -z ${qglviewerframework} ]; then
        install_name_tool -change ${qglviewerframework} @executable_path/../Frameworks/$( echo ${qglviewerframework} | sed -e "s/ //g" ) ${APP}
        set +e
        cp -r $QGLVIEWERROOT/usr/local/lib/QGLViewer.framework ../Frameworks/
        set -e
    fi
    cd -

    # WAR for OpenMesh
    cd install/bin/${APP}.app/Contents/MacOS
    libsOpenMesh='libOpenMeshCore libOpenMeshTools'
    for libOpenMesh in $libsOpenMesh; do
        set +e
        libOpenMeshFound=$(otool -L ${APP_EXE} | grep $libOpenMesh)
        set -e
        if [ ! -z "${libOpenMeshFound}" ]; then
            cp -r $OPENMESH_ROOT/lib/${libOpenMesh}*dylib* ../Frameworks/
        fi
    done
    cd -


    if [ -f ../.${APP}.deploy-extra ]; then
        for dir in `cat ../.${APP}.deploy-extra`; do
            cp -r ../${dir} install/bin/${APP}.app/Contents/MacOS/
        done
    fi

    # Create DMG
    hdiutil create -volname ${APP} -srcfolder install/bin/${APP}.app/ \
            -ov -format UDZO ${APP}-${APP_VERSION}-x86_64.dmg

    # Upload bundle to remote server
    ssh $(echo ${DMGAPP_REPO} | cut -d: -f1) mkdir -p $(echo ${DMGAPP_REPO} | cut -d: -f2)/${PROJECT_NAME}/${APP_VERSION}
    scp ${APP}-${APP_VERSION}-x86_64.dmg ${DMGAPP_REPO}/${PROJECT_NAME}/${APP_VERSION}

done # for each app
