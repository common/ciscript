# Ensure installation
cmake --build . --target install \
      --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}

# If no app defined use project name as default
if [ -z "${APPIMAGES}" ]; then
    APPIMAGES=${PROJECT_NAME};
fi

# Download latest linuxdeployqt
wget https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage
chmod +x linuxdeployqt-continuous-x86_64.AppImage


for APP in ${APPIMAGES}; do
    # Create structure for AppImage
    mkdir -p appdir.${APP}/usr/share/applications/
    cp ../.${APP}.desktop appdir.${APP}/usr/share/applications/${APP}.desktop

    mkdir -p appdir.${APP}/usr/bin
    # Copy executable defined in .desktop file
    cp -rp install/bin/$(cat appdir.${APP}/usr/share/applications/${APP}.desktop | grep Exec | cut -d"=" -f2) appdir.${APP}/usr/bin

    # Copy libs
    set +e
    cp -rp install/lib/ appdir.${APP}/usr/
    set -e

    # Copy icon
    mkdir -p appdir.${APP}/usr/share/icons
    cp ../${APPIMAGE_ICON} appdir.${APP}/usr/share/icons/
    cp ../${APPIMAGE_ICON} appdir.${APP}

    # Add default Qt5 to path in case QTDIR not setmotherwise use QTDIR/bin
    if [ -z ${QTDIR} ]; then
        export QTBINPATH=/usr/lib/x86_64-linux-gnu/qt5/bin/
    else
        export QTBINPATH=${QTDIR}/bin/
    fi

    if [ -f ../.${APP}.deploy-extra ]; then
        for dir in `cat ../.${APP}.deploy-extra`; do
            cp -r ../${dir} appdir.${APP}/usr/bin
            cd appdir.${APP}
            ln -s usr/bin/$(basename ${dir}) .
            cd ..
        done
    fi

    # Run linuxdeployqt
    QMAKE=$(PATH=$QTBINPATH:$PATH which qmake)
    PATH=$QTBINPATH:$PATH VERSION=${APP_VERSION} QT_PLUGIN_PATH= ./linuxdeployqt-continuous-x86_64.AppImage appdir.${APP}/usr/share/applications/*.desktop -appimage -qmake=$QMAKE

    # Create remote dir and upload AppImage to remote server
    ssh $(echo ${APPIMAGE_REPO} | cut -d: -f1) mkdir -p $(echo ${APPIMAGE_REPO} | cut -d: -f2)/${PROJECT_NAME}/${APP_VERSION}
    scp ${APP}-${APP_VERSION}-x86_64.AppImage ${APPIMAGE_REPO}/${PROJECT_NAME}/${APP_VERSION}
done
