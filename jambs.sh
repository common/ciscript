echo Jambs additional dependencies processing

#Download JAMBS and configs
# test ssh for debugging reasons
echo git clone https://gitlab.gmrv.es/common/jambs.git
git clone https://gitlab.gmrv.es/common/jambs.git
cd jambs
source ../.jambs_configs.sh

# Build dependencies via JAMBS for all build types
for BUILD_TYPE in Release; do
    GetShortBuildDir "${BUILD_TYPE}" "${BUILD_GENERATOR}"

    JAMBS_BUILD_DIR="${BUILD_DIR}"

    echo rm -rf "${BUILD_DIR}"
    rm -rf "${BUILD_DIR}"

    echo cmake -E make_directory "${BUILD_DIR}"
    cmake -E make_directory "${BUILD_DIR}"

    echo cd "${BUILD_DIR}"
    cd "${BUILD_DIR}"

    echo cmake \
         -DJAMBS_TARGETS="$JAMBS_TARGETS" \
         -DCMAKE_BUILD_TYPE="$BUILD_TYPE" \
         ${BUILD_GENERATOR_PREFIX}"$BUILD_GENERATOR" \
         ${CMAKE_EXTRA_ARGS} ..

    cmake \
        -DJAMBS_TARGETS="$JAMBS_TARGETS" \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE" \
        ${BUILD_GENERATOR_PREFIX}"$BUILD_GENERATOR" \
        ${CMAKE_EXTRA_ARGS} ..
    echo \
        cmake --build . --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}
    cmake --build . --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}

    cd ..
done

# Get out of JAMBS directory
cd ..
