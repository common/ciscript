APP_DIR=${PROJECT_NAME}-${APP_VERSION}-Win-x86_64
mkdir ${APP_DIR}

# if not WINEXES defined then use project name
if [ -z ${WINEXES} ]; then
    WINEXES=${PROJECT_NAME}
fi

for APP in ${WINEXES}; do
    cp bin/Release/${APP}.exe ${APP_DIR}/
done

set +e
cp bin/Release/*.dll ${APP_DIR}/
set -e

# Copy extra dlls listed in .dlls file that windeployqt does not copy
if [ -f ../.dlls ]; then
    for dll in `cat ../.dlls`; do
        dll_file="echo $dll"
        cp "$(eval $dll_file)" ${APP_DIR}
    done
fi

if [ -f ../.${PROJECT_NAME}.deploy-extra ]; then
    for dir in `cat ../.${PROJECT_NAME}.deploy-extra | sed -e "s/\r//"`; do
        cp -r ../${dir} ${APP_DIR}
    done
fi

# Qt deploy
cd ${APP_DIR}
for APP in ${WINEXES}; do
    ${QTDIR}/bin/windeployqt.exe ${APP}.exe
done
cd ..

# Make zip package
zip -r ${APP_DIR}.zip ${APP_DIR}

# Upload app to remote server
ssh -oStrictHostKeyChecking=no $(echo ${WINAPP_REPO} | cut -d: -f1) mkdir -p $(echo ${WINAPP_REPO} | cut -d: -f2)/${PROJECT_NAME}/${APP_VERSION}
scp ${APP_DIR}.zip ${WINAPP_REPO}/${PROJECT_NAME}/${APP_VERSION}
