##############################################################
#
# Building script for gitlabci
# Grupo de Modelado y Realidad Virtual (GMRV)
# Universidad Rey Juan Carlos - URJC
#
##############################################################


# Variables to be set before calling

# BUILD_GENERATOR:
#   CMake generator to be used.
#   For example: Unix Makefiles
#   If empty system's default is used

# BUILD_OPTIONALS_SUBPROJECTS:
#   If defined, it will try to uncomment optional subprojects from
#   the .gitsubproject file

# JAMBS_TARGETS:
#   [DEPRECATED] dependencies to be build (semicolon separated).
#   Reads config files from .jambs_configs
#   If empty no extra dependencies will be built using jambs

# GENERATOR_EXTRA_OPTIONS:
#   Options to be passed to the native generator (for example -j4)

# CMAKE_EXTRA_ARGS:
#   Additional arguments for CMake (for example -DTESTS=ON)

# CMAKE_EXTRA_TARGETS:
#   Additional targets to run after build (for example tests)

# BUILD_TYPE_LIST:
#   Space separated list of cmake build types.
#   If empty Debug and Release types will be built

# COVERAGE:
#   If this variable is not empty coverage will be computed and its value
#   will be present in the build log.

# DEPLOY_DOC_REPO:
#   Reposiroty URL for documentation. If it has a value this script
#   will run Doxygen targen and commit changes to the URL defined.

# GITHUB_MIRROR_URL:
#   If set to a valid URL the script will try to push the changes to that URL

# APPIMAGE_REPO and APPIMAGE_ICON:
#   If both set, if the build is done in Linux, an AppImage will be created
#   and pushed 

# WINAPP_REPO
#   If set, and the build is done in Windows, a zip with binaries will be
#   created and pushed 

# DMGAPP_REPO
#   If set, and the build is done in OSX, an OSX bundle will be
#   created and pushed

# SKIP_CMAKE_BUILD
#   If set skips the build process and just trie to push in GITHUB_MIRROR_URL.
#   (for example to push a Python project)


# Print input variables for logging purposes
echo BUILD_GENERATOR: ${BUILD_GENERATOR}
echo BUILD_OPTIONALS_SUBPROJECTS: ${BUILD_OPTIONALS_SUBPROJECTS}
echo JAMBS_TARGETS: ${JAMBS_TARGETS}
echo GENERATOR_EXTRA_OPTIONS: ${GENERATOR_EXTRA_OPTIONS}
echo CMAKE_EXTRA_ARGS: ${CMAKE_EXTRA_ARGS}
echo CMAKE_EXTRA_TARGETS: ${CMAKE_EXTRA_TARGETS}
echo BUILD_TYPE_LIST: ${BUILD_TYPE_LIST}
echo COVERAGE: ${COVERAGE}
echo GITHUB_MIRROR_URL: ${GITHUB_MIRROR_URL}
echo APPIMAGE_REPO: ${APPIMAGE_REPO}
echo APPIMAGE_ICON: ${APPIMAGE_ICON}
echo WINAPP_REPO: ${WINAPP_REPO}
echo DMGAPP_REPO: ${DMGAPP_REPO}
echo SKIP_CMAKE_BUILD: ${SKIP_CMAKE_BUILD}
  
echo CMake version
cmake --version

# Exit if something goes wrong with non-zero return value
set -e

# Set trace on
set -o xtrace


function GetShortBuildDir {

  __BUILD_TYPE__="$1"
  __GENERATOR__="$2"

  echo ${__GENERATOR__}

  if [[ -z __BUILD_TYPE__ ]]; then
      echo GetShortBuildDir: no __BUILD_TYPE__ defined
      exit -1
  fi

  if [[ -z __GENERATOR__ ]]; then
      echo GetShortBuildDir: no __GENERATOR__ defined
      exit -1
  fi

  if [ "${__BUILD_TYPE__}" == "Debug" ]; then
      __SHORT_BUILD_DIR_NAME__="D"
  else
      if [ "${__BUILD_TYPE__}" == "Release" ]; then
          __SHORT_BUILD_DIR_NAME__="R"
      else
          echo GetShortBuildDir: ${__BUILD_TYPE__} not supported
          exit -1
      fi
  fi

  if [ "${__GENERATOR__}" == "Visual Studio 10" ]; then
      __SHORT_BUILD_DIR_NAME__=${__SHORT_BUILD_DIR_NAME__}".vs10x86"
  else
      if [ "${__GENERATOR__}" == "Visual Studio 10 Win64" ]; then
          __SHORT_BUILD_DIR_NAME__=${__SHORT_BUILD_DIR_NAME__}".vs10x64"
      else
          if [ "${__GENERATOR__}" == "Visual Studio 12" ]; then
              __SHORT_BUILD_DIR_NAME__=${__SHORT_BUILD_DIR_NAME__}".vs12x86"
          else
              if [ "${__GENERATOR__}" == "Visual Studio 12 Win64" ]; then
                  __SHORT_BUILD_DIR_NAME__=${__SHORT_BUILD_DIR_NAME__}".vs12x64"
              else
                  if [ "${__GENERATOR__}" == "Visual Studio 14" ]; then
                      __SHORT_BUILD_DIR_NAME__=${__SHORT_BUILD_DIR_NAME__}".vs14x86"
                  else
                      if [ "${__GENERATOR__}" == "Visual Studio 14 Win64" ]; then
                          __SHORT_BUILD_DIR_NAME__=${__SHORT_BUILD_DIR_NAME__}".vs14x64"
                      fi
                  fi
              fi
          fi
      fi
  fi

  BUILD_DIR=ci.${__SHORT_BUILD_DIR_NAME__}

}

if ! [ -v SKIP_CMAKE_BUILD ]; then
    # Get OS type and get first part for Windows/bash
    OSTYPE=$(uname | cut -d_ -f1 )

    PROJECT_NAME=`grep project CMakeLists.txt | grep VERSION | cut -d"(" -f2 | sed -e "s/ //" | cut -d " " -f1 | sed -e "s/VERSION//"`
    PROJECT_VERSION=$(grep VERSION CMakeLists.txt  | grep project | sed 's/.*VERSION //' | cut -d ")" -f1 | cut -d" " -f1)

    # Update submodules
    git submodule update --init

    # Delete possible traces from old builds
    rm -rf jambs Debug* Release*

    if [[ -z "${BUILD_TYPE_LIST}" ]]; then
        BUILD_TYPE_LIST="Debug Release"
    fi

    if [[ -n "${BUILD_GENERATOR}" ]]; then
        BUILD_GENERATOR_PREFIX=-G
    fi

    if [[ -n ${BUILD_OPTIONALS_SUBPROJECTS} ]]; then
        if [[ -f .gitsubprojects ]]; then
            ORIG_FILE=.gitsubprojects.orig.ci
            mv .gitsubprojects ${ORIG_FILE}
            head -1 ${ORIG_FILE} > .gitsubprojects
            grep git_subproject ${ORIG_FILE} | sed -e "s/#//" >> .gitsubprojects
        fi
    fi

    if [[ -n "${JAMBS_TARGETS}" ]]; then
        source jambs.sh
    fi

    # Build project
    for BUILD_TYPE in ${BUILD_TYPE_LIST}; do
        GetShortBuildDir "${BUILD_TYPE}" "${BUILD_GENERATOR}"

        echo rm -rf "${BUILD_DIR}"
        rm -rf "${BUILD_DIR}"

        echo cmake -E make_directory "${BUILD_DIR}"
        cmake -E make_directory "${BUILD_DIR}"

        echo cd "${BUILD_DIR}"
        cd "${BUILD_DIR}"

        if [[ ! -z ${COVERAGE} ]]; then
            echo "Enabling coverage"
            CMAKE_EXTRA_ARGS=${CMAKE_EXTRA_ARGS}" -DCOMMON_ENABLE_COVERAGE=ON "
            CMAKE_EXTRA_TARGETS=${CMAKE_EXTRA_TARGETS}" "${PROJECT_NAME}"-coverage"
        fi

        CMAKE_EXTRA_ARGS=${CMAKE_EXTRA_ARGS}" -DCOMMON_SOURCE_DIR=`pwd`/../commonsrc"

        echo cmake \
              -DCMAKE_BUILD_TYPE="$BUILD_TYPE" \
              ${BUILD_GENERATOR_PREFIX}"$BUILD_GENERATOR" \
              -DCMAKE_PREFIX_PATH="`pwd`/../jambs/${JAMBS_BUILD_DIR}/install" \
              -DCMAKE_INSTALL_PREFIX=install \
              -DLCOV_EXCLUDE="\"*/commonsrc/*\"" \
              ${CMAKE_EXTRA_ARGS} ..

        cmake \
              -DCMAKE_BUILD_TYPE="$BUILD_TYPE" \
              ${BUILD_GENERATOR_PREFIX}"$BUILD_GENERATOR" \
              -DCMAKE_PREFIX_PATH="`pwd`/../jambs/${JAMBS_BUILD_DIR}/install" \
              -DCMAKE_INSTALL_PREFIX=install \
              -DLCOV_EXCLUDE="\"*/commonsrc/*\"" \
              ${CMAKE_EXTRA_ARGS} ..

        echo cmake --build . --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}
        cmake --build . --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}

        for extraTarget in ${CMAKE_EXTRA_TARGETS}; do
            echo cmake --build . --target $extraTarget \
                 --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}
            cmake --build . --target $extraTarget \
                  --config ${BUILD_TYPE} -- ${GENERATOR_EXTRA_OPTIONS}
            if [ "${BUILD_TYPE}" = "Debug" ] && [ "${COVERAGE}" = "ON" ]; then
                echo "("`cat doc/html/CoverageReport/index.html | grep  headerCovTableEntryMed | head -n1 | cut -d \> -f2 | cut -d" " -f1`"%)" covered
            fi


            if [ "${BUILD_TYPE}" = "Release" ] && [ ! -z ${DEPLOY_DOC_REPO} ] && [ "${extraTarget}" = "doxygen" ]; then
                git clone ${DEPLOY_DOC_REPO} ___DOC_REPO___
                cd ___DOC_REPO___
                mkdir -p ${PROJECT_NAME}
                mkdir -p doc/${PROJECT_NAME}
                cp -r ../doc/html/* doc/${PROJECT_NAME}
                git add doc/${PROJECT_NAME}
                git commit -m "Buildbot updated doc for ${PROJECT_NAME}"
                git push origin master
                cd ..
            fi
        done # For all extra targets

        # If APP_VERSION not set, if its nightly build set version
        # accordingly otherwise set app version to project version
        if [ -z ${APP_VERSION} ]; then
            if [ ! -z ${NIGHTLY_BUILD_NAME} ]; then
                APP_VERSION=${NIGHTLY_BUILD_NAME}
            else
                APP_VERSION=${PROJECT_VERSION}
            fi
        fi

        # If APP_VERSION set to dev, append date of last commit and git head rev
        if [ "${APP_VERSION}" = "dev" ]; then
          if [ "$(uname)" = "Darwin" ]; then
            APP_VERSION="dev-$(date -r $(git log -1 --format=%ct)  '+%Y%m%d-%H%M')-$(git rev-parse --short HEAD)"
          else
            APP_VERSION="dev-$(date -d @$(git log -1 --format=%ct)  '+%Y%m%d-%H%M')-$(git rev-parse --short HEAD)"
          fi
        fi

        ##
        ## AppImage creation and upload
        ##
        if [ "${BUILD_TYPE}" = "Release" ] && [ "${OSTYPE}" = "Linux" ] && [ ! -z "${APPIMAGE_REPO}" ] && [ ! -z "${APPIMAGE_ICON}" ]; then
            source ../.ciscript/appimage.sh
        fi # AppImage

        if [ "${BUILD_TYPE}" = "Release" ] && [ "${OSTYPE}" = "Linux" ] && [ ! -z "${DOCKER_REPO}" ]; then
            source ../.ciscript/docker.sh
        fi # AppImage

        ##
        ## Windows app bundle
        ##
        if [ "${BUILD_TYPE}" = "Release" ] && [ "${OSTYPE}" = "MINGW64" ] && [ ! -z "${WINAPP_REPO}" ]; then
            source ../.ciscript/winapp.sh
        fi # Windows app bundle

        ##
        ## OSX DMG bundle
        ##
        if [ "${BUILD_TYPE}" = "Release" ] && [ "${OSTYPE}" = "Darwin" ] && [ ! -z "${DMGAPP_REPO}" ]; then
            source ../.ciscript/osxbundle.sh
        fi # OSX DMG bundle

        cd ..

    done
fi #SKIP_CMAKE_BUILD

if [ ! -z "${GITHUB_MIRROR_URL}" ]; then
    rm -rf ___GITLAB_REPO___
    git clone git@`git remote -v | grep origin | cut -d "@" -f2 | sed -e "s/\//:/" | cut -d" " -f1 | head -n1` ___GITLAB_REPO___
    cd ___GITLAB_REPO___
    git remote add github ${GITHUB_MIRROR_URL}
    git push github master
fi
